const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'GAMETIE LABS',
  tagline: 'Bringing the game to everyones life.',
  url: 'https://gametielabs.gitlab.io/',

  baseUrl: '/gametielabs/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'Gametie Labs company', // Usually your GitHub org/user name.
  projectName: 'gametielabs', // Usually your repo name.
  themeConfig: {
    hideableSidebar: false,
colorMode: {
  defaultMode: 'light',
  disableSwitch: true,
  respectPrefersColorScheme: true,
  switchConfig: {
    darkIcon: '�',
    // lightIcon: '\u2600',
    // React inline style object
    // see https://reactjs.org/docs/dom-elements.html#style
    darkIconStyle: {
      marginLeft: '2px',
    },
    lightIconStyle: {
      marginLeft: '1px',
    },
  },
},
    navbar: {
      title: 'Gametie Labs',
      style: 'dark',
      logo: {
        alt: '',
        src: 'img/gametielabs.png',
      },
      items: [
        // {
        //   type: 'doc',
        //   docId: 'intro',
        //   position: 'left',
        //   label: 'Tutorial',
        // },
        // {to: '/blog', label: 'Blog', position: 'left'},
        // {
        //   href: 'https://youtube.com/learnersnation',
        //   label: 'YouTube',
        //   position: 'right',
        // },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Stay Connected',
          items: [
            {
              label: 'Youtube',
              href: 'https://www.youtube.com/LearnersNation',
            },
            {
              label: 'Facebook',
              href: 'https://www.youtube.com/LearnersNation',
            },
            {
              label: 'Twitter',
              href: 'https://www.youtube.com/LearnersNation',
            },
            {
              label: 'Discord',
              href: 'https://www.youtube.com/LearnersNation',
            },
            {
              label: 'Reddit',
              href: 'https://www.youtube.com/LearnersNation',
            },
          ],
        },
        {
          title: 'Support and Policy',
          items: [
            {
              label: 'Terms and Conditions',
              href: '/docs/terms',
            },
            {
              label: 'Privacy Policy',
              href: '/docs/privacy',
            },
            {
              label: 'Contact me',
              href: '/docs/contact',
            },
          ],
        },
        {
          // title: 'More',
          // items: [
          //   {
          //     label: 'Blog',
          //     to: '/blog',
          //   },
          //   {
          //     label: 'Youtube',
          //     href: 'https://www.youtube.com/LearnersNation',
          //   },
          // ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Gametie Labs. All rights reserved.`,
    },
    prism: {
      theme: lightCodeTheme,
      darkTheme: darkCodeTheme,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
