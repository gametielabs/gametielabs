import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import styles from './index.module.css';
import HomepageFeatures from '../components/HomepageFeatures';
// import background from "./banner.png";
// import WebFont from 'webfontloader';
import './index.css';
// WebFont.load({
//   google: {
//     families: ['Padauk A1:300,400,700', 'sans-serif']
//   }
// });

// <img src={background}/>
function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (

    <header className={clsx('hero hero--primary', styles.heroBanner)}>

      <div className="container">

        <h1 className="hero__title">{siteConfig.title}</h1>

        <h3 className="hero__subtitle">{siteConfig.tagline}</h3>

      </div>

    </header>
  );
}

export default function Home() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={`Welcome to `}//{`Hello from ${siteConfig.title}`}
      description="Indie game developer <head />">
      <HomepageHeader />
      <main>
        <HomepageFeatures />
    </main>
    </Layout>
  );
}
