import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';
import Link from '@docusaurus/Link';

const FeatureList = [
  {
    title: 'WORLD OF BOATS',
    Svg: require('../../static/img/world-of-boats.svg').default,
    description: (
      <>
Compete against drivers of powerboats from around the world.
Race with powerboat racers and evade a relentless police force while you clash.
Buy new powerboats, assemble a garage full of powerboats and race your way to the top of the League.
      </>
    ),
    platform: 'Android, iOS',
    featureLink: 'docs/privacy',
  },
  {
    title: 'JEWEL PUZZLE BLAST',
    Svg: require('../../static/img/jewel-blast-puzzle.svg').default,
    description: (
      <>
        Are you ready to challenge? Let us be the master of  puzzle and sharpen the brain together! A new classic block match game, 100% free, no ads, no money!
        Challenge your friends and beat them with your high score.
      </>
    ),
    platform: 'Android, iOS',
    featureLink: 'docs/terms',
  },
  {
    title: 'WORLD OF FRUITS',
    Svg: require('../../static/img/world-of-fruits.svg').default,
    description: (
      <>
      This is a brand new and innovative fruit game.
      Link fruits by swiping 3 or more fruits
      Tasty and juicy fruit graphics
      Hundreds of unique puzzles that are full of amazing challenges!
      </>
    ),
    platform: 'Android, iOS',
    featureLink: 'docs/terms',
  },
];

function Feature({Svg, title, description,platform,featureLink}) {
  return (
    <div className={clsx('col col--4')}>
    <Link to ={featureLink}>

      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
          <h3 className={styles.featureH3}><b>{title}</b></h3>
      </div>
</Link>
      <div className="text--center padding-horiz--md">

        <h4 className={styles.featureH4}><b>{platform}</b></h4>
        <p>{description}</p>
      </div>

    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
